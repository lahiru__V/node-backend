const express = require('express');
const router = express.Router();
const enrollmentController = require('../controllers/enrollmentController');

router.post('/enrollments', enrollmentController.enrollStudent);
router.delete('/enrollments/:id', enrollmentController.dropStudent);

module.exports = router;
