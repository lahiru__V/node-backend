const express = require('express');
const router = express.Router();
const timetableController = require('../controllers/timetableController');

router.post('/timetables', timetableController.createTimetable);
router.put('/timetables/:id', timetableController.updateTimetable);
router.delete('/timetables/:id', timetableController.deleteTimetable);

module.exports = router;
