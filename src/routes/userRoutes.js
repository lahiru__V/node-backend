const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const { authenticateUser } = require('../middleware/authMiddleware');

router.post('/register', userController.registerUser);
router.post('/login', userController.loginUser);
router.post('/logout', authenticateUser, userController.logoutUser);
router.get('/profile', authenticateUser, userController.getUserProfile);

module.exports = router;
