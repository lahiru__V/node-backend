const Faculty = require('../models/faculty');

exports.createFaculty = async (facultyData) => {
    const faculty = new Faculty(facultyData);
    await faculty.save();
    return faculty;
};

exports.getFaculties = async () => {
    return await Faculty.find();
};

exports.getFacultyById = async (facultyId) => {
    return await Faculty.findById(facultyId);
};

exports.updateFaculty = async (id, facultyData) => {
    return await Faculty.findByIdAndUpdate(id, facultyData, { new: true, runValidators: true });
};

exports.deleteFaculty = async (id) => {
    return await Faculty.findByIdAndDelete(id);
};
