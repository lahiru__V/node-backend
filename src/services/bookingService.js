const Booking = require('../models/booking');

exports.bookRoomResource = async (bookingData) => {  
  const existingBooking = await Booking.findOne({
    resource: bookingData.resource,
    date: bookingData.date,
    time: bookingData.time
  });

  if (existingBooking) {
    throw new Error('Another booking already exists for the same resource at the same time.');
  }

  const booking = new Booking(bookingData);
  await booking.save();
  return booking;
};

exports.updateBooking = async (id, bookingData) => {  
  const existingBooking = await Booking.findOne({
    _id: { $ne: id }, 
    resource: bookingData.resource,
    date: bookingData.date,
    time: bookingData.time
  });

  if (existingBooking) {
    throw new Error('Another booking already exists for the same resource at the same time.');
  }

  const booking = await Booking.findByIdAndUpdate(id, bookingData, { new: true, runValidators: true });
  if (!booking) {
    throw new Error('Booking not found');
  }
  return booking;
};

exports.cancelBooking = async (id) => {
  const booking = await Booking.findByIdAndDelete(id);
  if (!booking) {
    throw new Error('Booking not found');
  }
  return booking;
};