const Timetable = require('../models/timetable');

exports.createTimetable = async (timetableData) => {  
  const existingTimetable = await Timetable.findOne({
    location: timetableData.location,
    time: timetableData.time
  });
  if (existingTimetable) {
    throw new Error('Another timetable already exists for the same location and time.');
  }
  const timetable = new Timetable(timetableData);
  await timetable.save();
  return timetable;
};

exports.updateTimetable = async (id, timetableData) => {  
  const existingTimetable = await Timetable.findOne({
    _id: { $ne: id }, 
    location: timetableData.location,
    time: timetableData.time
  });

  if (existingTimetable) {
    throw new Error('Another timetable already exists for the same location and time.');
  }

  const timetable = await Timetable.findByIdAndUpdate(id, timetableData, { new: true, runValidators: true });
  if (!timetable) {
    throw new Error('Timetable not found');
  }
  return timetable;
};

exports.getTimetable = async (courseId) => {
  const timetable = await Timetable.findOne({ course: courseId });
  return timetable;
};

exports.deleteTimetable = async (id) => {
  const timetable = await Timetable.findByIdAndDelete(id);
  if (!timetable) {
    throw new Error('Timetable not found');
  }
  return timetable;
};