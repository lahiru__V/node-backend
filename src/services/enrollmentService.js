const Enrollment = require('../models/enrollment');

exports.enrollStudent = async (enrollmentData) => {
  const enrollment = new Enrollment(enrollmentData);
  await enrollment.save();
  return enrollment;
};

exports.dropStudent = async (student, course) => {
  const enrollment = await Enrollment.findOneAndDelete({ student, course });
  if (!enrollment) {
    throw new Error('Enrollment not found');
  }
  return enrollment;
};