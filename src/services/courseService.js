const Course = require('../models/course');

exports.createCourse = async (courseData) => {
  const course = new Course(courseData);
  await course.save();
  return course;
};

exports.getCourses = async () => {
  const courses = await Course.find();
  return courses;
};

exports.getCourseById = async (courseId) => {
  const course = await Course.findById(courseId);
  if (!course) {
    throw new Error('Course not found');
  }
  return course;
};

exports.updateCourse = async (id, courseData) => {
  const course = await Course.findByIdAndUpdate(id, courseData, { new: true, runValidators: true });
  if (!course) {
    throw new Error('Course not found');
  }
  return course;
};

exports.deleteCourse = async (id) => {
  const course = await Course.findByIdAndDelete(id);
  if (!course) {
    throw new Error('Course not found');
  }
  return course;
};

exports.assignFaculty = async (id, faculty) => {
  const course = await Course.findById(id);
  if (!course) {
    throw new Error('Course not found');
  }
  course.faculty = faculty;
  await course.save();
  return course;
};