const Notification = require('../models/notification');

exports.sendNotification = async (notificationData) => {
  const notification = new Notification(notificationData);
  await notification.save();
  return notification;
};

exports.getNotifications = async (recipientId) => {
  const notifications = await Notification.find({ recipient: recipientId });
  return notifications;
};