exports.getCurrentDateTime = () => {
    return new Date();
  };
  
  exports.formatDateTime = (dateTime) => {
    return dateTime.toLocaleString(); 
  };
  