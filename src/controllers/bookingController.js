const bookingService = require('../services/bookingService');

exports.bookRoomResource = async (req, res) => {
  try {
    const booking = await bookingService.bookRoomResource(req.body);
    res.status(201).send(booking);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

exports.updateBooking = async (req, res) => {
  try {
    const booking = await bookingService.updateBooking(req.params.id, req.body);
    if (!booking) {
      return res.status(404).send();
    }
    res.send(booking);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

exports.cancelBooking = async (req, res) => {
  try {
    const booking = await bookingService.cancelBooking(req.params.id);
    if (!booking) {
      return res.status(404).send();
    }
    res.send(booking);
  } catch (err) {
    res.status(500).send(err.message);
  }
};
