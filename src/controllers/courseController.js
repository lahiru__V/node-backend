const courseService = require('../services/courseService');

exports.createCourse = async (req, res) => {
  try {
    const course = await courseService.createCourse(req.body);
    res.status(201).send(course);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

exports.getCourses = async (req, res) => {
  try {
    const courses = await courseService.getCourses();
    res.send(courses);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.getCourseById = async (req, res) => {
  try {
    const course = await courseService.getCourseById(req.params.id);
    res.send(course);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.updateCourse = async (req, res) => {
  try {
    const course = await courseService.updateCourse(req.params.id, req.body);
    res.send(course);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

exports.deleteCourse = async (req, res) => {
  try {
    await courseService.deleteCourse(req.params.id);
    res.status(204).send();
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.assignFaculty = async (req, res) => {
  try {
    const course = await courseService.assignFaculty(req.params.id, req.body.faculty);
    res.send(course);
  } catch (err) {
    res.status(400).send(err.message);
  }
};
