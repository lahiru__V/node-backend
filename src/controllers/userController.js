const userService = require('../services/userService');

exports.registerUser = async (req, res) => {
  try {
    const user = await userService.registerUser(req.body);
    res.status(201).send(user);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

exports.loginUser = async (req, res) => {
  try {
    const { user, token } = await userService.loginUser(req.body.username, req.body.password);
    res.send({ user, token });
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.logoutUser = async (req, res) => {
  try {
    await userService.logoutUser(req.user, req.token);
    res.send();
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.getUserProfile = async (req, res) => {
  try {
    const user = await userService.getUserProfile(req.user._id);
    res.send(user);
  } catch (err) {
    res.status(500).send(err.message);
  }
};
