const notificationService = require('../services/notificationService');

exports.sendNotification = async (req, res) => {
  try {
    const notification = await notificationService.sendNotification(req.body);
    res.status(201).send(notification);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

exports.getNotifications = async (req, res) => {
  try {
    const notifications = await notificationService.getNotifications(req.params.id);
    res.send(notifications);
  } catch (err) {
    res.status(500).send(err.message);
  }
};
