const enrollmentService = require('../services/enrollmentService');

exports.enrollStudent = async (req, res) => {
  try {
    const enrollment = await enrollmentService.enrollStudent(req.body);
    res.status(201).send(enrollment);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

exports.dropStudent = async (req, res) => {
  try {
    const enrollment = await enrollmentService.dropStudent(req.body.student, req.body.course);
    if (!enrollment) {
      return res.status(404).send();
    }
    res.send(enrollment);
  } catch (err) {
    res.status(500).send(err.message);
  }
};
