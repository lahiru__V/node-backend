const facultyService = require('../services/facultyService');

exports.createFaculty = async (req, res) => {
    try {
        const faculty = await facultyService.createFaculty(req.body);
        res.status(201).send(faculty);
    } catch (err) {
        res.status(400).send(err.message);
    }
};

exports.getFaculties = async (req, res) => {
    try {
        const faculties = await facultyService.getFaculties();
        res.send(faculties);
    } catch (err) {
        res.status(500).send(err.message);
    }
};

exports.getFacultyById = async (req, res) => {
    try {
        const faculty = await facultyService.getFacultyById(req.params.id);
        if (!faculty) {
            return res.status(404).send();
        }
        res.send(faculty);
    } catch (err) {
        res.status(500).send(err.message);
    }
};

exports.updateFaculty = async (req, res) => {
    try {
        const faculty = await facultyService.updateFaculty(req.params.id, req.body);
        if (!faculty) {
            return res.status(404).send();
        }
        res.send(faculty);
    } catch (err) {
        res.status(400).send(err.message);
    }
};

exports.deleteFaculty = async (req, res) => {
    try {
        const faculty = await facultyService.deleteFaculty(req.params.id);
        if (!faculty) {
            return res.status(404).send();
        }
        res.status(204).send();
    } catch (err) {
        res.status(500).send(err.message);
    }
};
