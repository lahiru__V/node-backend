const timetableService = require('../services/timetableService');

exports.createTimetable = async (req, res) => {
  try {
    const timetable = await timetableService.createTimetable(req.body);
    res.status(201).send(timetable);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

exports.updateTimetable = async (req, res) => {
  try {
    const timetable = await timetableService.updateTimetable(req.params.id, req.body);
    if (!timetable) {
      return res.status(404).send();
    }
    res.send(timetable);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

exports.deleteTimetable = async (req, res) => {
  try {
    const timetable = await timetableService.deleteTimetable(req.params.id);
    if (!timetable) {
      return res.status(404).send();
    }
    res.send(timetable);
  } catch (err) {
    res.status(500).send(err.message);
  }
};
