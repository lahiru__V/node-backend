const mongoose = require('mongoose');

const timetableSchema = new mongoose.Schema({
  course: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Course'
  },
  day: {
    type: String,
    required: true
  },
  time: {
    type: String,
    required: true
  },
  faculty: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  location: String
});

const Timetable = mongoose.model('Timetable', timetableSchema);

module.exports = Timetable;
