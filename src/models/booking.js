const mongoose = require('mongoose');

const bookingSchema = new mongoose.Schema({
  resource: String,
  date: {
    type: Date,
    required: true
  },
  time: {
    type: String,
    required: true
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User' 
  }
});

const Booking = mongoose.model('Booking', bookingSchema);

module.exports = Booking;
