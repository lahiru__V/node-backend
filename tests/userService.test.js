const userService = require('../src/services/userService');
const User = require('../src/models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

jest.mock('../src/models/user');
jest.mock('bcrypt');
jest.mock('jsonwebtoken');

describe('User Service', () => {
  beforeEach(() => {
    User.mockClear();
    bcrypt.hash.mockClear();
    bcrypt.compare.mockClear();
    jwt.sign.mockClear();
  });

  describe('registerUser', () => {
    it('should register a new user', async () => {
      const userData = { username: 'testuser', email: 'test@example.com', password: 'password', role: 'Student' };
      const hashedPassword = 'hashedpassword';
      const newUser = new User(userData);
      bcrypt.hash.mockResolvedValueOnce(hashedPassword);
      User.mockReturnValueOnce(newUser);

      const result = await userService.registerUser(userData);

      expect(bcrypt.hash).toHaveBeenCalledWith(userData.password, 10);
      expect(User).toHaveBeenCalledWith({ ...userData, password: hashedPassword });
      expect(newUser.save).toHaveBeenCalled();
      expect(result).toEqual(newUser);
    });
  });

  describe('loginUser', () => {
    it('should log in an existing user with correct credentials', async () => {
      const username = 'testuser';
      const password = 'password';
      const user = new User({ username, email: 'test@example.com', password: 'hashedpassword', role: 'Student' });
      User.findOne.mockResolvedValueOnce(user);
      bcrypt.compare.mockResolvedValueOnce(true);
      const token = 'token';
      jwt.sign.mockReturnValueOnce(token);

      const result = await userService.loginUser(username, password);

      expect(User.findOne).toHaveBeenCalledWith({ username });
      expect(bcrypt.compare).toHaveBeenCalledWith(password, user.password);
      expect(jwt.sign).toHaveBeenCalledWith({ userId: user._id }, process.env.JWT_SECRET, { expiresIn: '1h' });
      expect(result).toEqual({ user, token });
    });

    it('should throw an error for invalid username or password', async () => {
      const username = 'testuser';
      const password = 'password';
      User.findOne.mockResolvedValueOnce(null);

      await expect(userService.loginUser(username, password)).rejects.toThrow('User not found');

      const user = new User({ username, email: 'test@example.com', password: 'hashedpassword', role: 'Student' });
      User.findOne.mockResolvedValueOnce(user);
      bcrypt.compare.mockResolvedValueOnce(false);

      await expect(userService.loginUser(username, password)).rejects.toThrow('Invalid password');
    });
  });

  describe('logoutUser', () => {
    it('should log out a user by removing the token', async () => {
      const user = new User({ username: 'testuser', email: 'test@example.com', password: 'hashedpassword', role: 'Student' });
      const token = 'token';

      await userService.logoutUser(user, token);

      expect(user.tokens).not.toContain(token);
      expect(user.save).toHaveBeenCalled();
    });
  });

  describe('getUserProfile', () => {
    it('should get user profile by id', async () => {
      const userId = '123456789';
      const user = new User({ _id: userId, username: 'testuser', email: 'test@example.com', password: 'hashedpassword', role: 'Student' });
      User.findById.mockResolvedValueOnce(user);

      const result = await userService.getUserProfile(userId);

      expect(User.findById).toHaveBeenCalledWith(userId);
      expect(result).toEqual(user);
    });

    it('should throw an error if user is not found by id', async () => {
      const userId = '123456789';
      User.findById.mockResolvedValueOnce(null);

      await expect(userService.getUserProfile(userId)).rejects.toThrow('User not found');
    });
  });
});
