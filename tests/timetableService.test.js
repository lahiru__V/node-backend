const timetableService = require('../src/services/timetableService');
const Timetable = require('../src/models/timetable');

jest.mock('../src/models/timetable');

describe('Timetable Service', () => {
  beforeEach(() => {
    Timetable.mockClear();
  });

  describe('createTimetable', () => {
    it('should create a new timetable', async () => {
      const timetableData = { course: '123456789', day: 'Monday', time: '10:00 AM', faculty: '987654321', location: 'Room 101' };
      const newTimetable = new Timetable(timetableData);
      Timetable.mockReturnValueOnce(newTimetable);

      const createdTimetable = await timetableService.createTimetable(timetableData);

      expect(Timetable).toHaveBeenCalledWith(timetableData);
      expect(newTimetable.save).toHaveBeenCalled();
      expect(createdTimetable).toEqual(newTimetable);
    });
  });

  describe('getTimetable', () => {
    it('should get timetable for a course', async () => {
      const courseId = '123456789';
      const timetable = new Timetable({ course: courseId, day: 'Monday', time: '10:00 AM', faculty: '987654321', location: 'Room 101' });
      Timetable.findOne.mockResolvedValueOnce(timetable);

      const result = await timetableService.getTimetable(courseId);

      expect(result).toEqual(timetable);
    });
  });

  describe('updateTimetable', () => {
    it('should update an existing timetable', async () => {
      const timetableId = '123456789';
      const timetableData = { day: 'Tuesday', time: '11:00 AM' };
      const updatedTimetable = new Timetable({ _id: timetableId, ...timetableData });
      Timetable.findByIdAndUpdate.mockResolvedValueOnce(updatedTimetable);

      const result = await timetableService.updateTimetable(timetableId, timetableData);

      expect(Timetable.findByIdAndUpdate).toHaveBeenCalledWith(timetableId, timetableData, { new: true, runValidators: true });
      expect(result).toEqual(updatedTimetable);
    });

    it('should throw an error if timetable is not found during update', async () => {
      const timetableId = '123456789';
      const timetableData = { day: 'Tuesday', time: '11:00 AM' };
      Timetable.findByIdAndUpdate.mockResolvedValueOnce(null);

      await expect(timetableService.updateTimetable(timetableId, timetableData)).rejects.toThrow('Timetable not found');
    });
  });

  describe('deleteTimetable', () => {
    it('should delete an existing timetable', async () => {
      const timetableId = '123456789';
      const deletedTimetable = new Timetable({ _id: timetableId, day: 'Monday', time: '10:00 AM', faculty: '987654321', location: 'Room 101' });
      Timetable.findByIdAndDelete.mockResolvedValueOnce(deletedTimetable);

      const result = await timetableService.deleteTimetable(timetableId);

      expect(Timetable.findByIdAndDelete).toHaveBeenCalledWith(timetableId);
      expect(result).toEqual(deletedTimetable);
    });

    it('should throw an error if timetable is not found during deletion', async () => {
      const timetableId = '123456789';
      Timetable.findByIdAndDelete.mockResolvedValueOnce(null);

      await expect(timetableService.deleteTimetable(timetableId)).rejects.toThrow('Timetable not found');
    });
  });
});