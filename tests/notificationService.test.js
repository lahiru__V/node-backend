const notificationService = require('../src/services/notificationService');
const Notification = require('../src/models/notification');

jest.mock('../src/models/notification');

describe('Notification Service', () => {
  beforeEach(() => {
    Notification.mockClear();
  });

  describe('sendNotification', () => {
    it('should send a notification', async () => {
      const notificationData = { message: 'Test message', type: 'info', recipient: '123456789' };
      const newNotification = new Notification(notificationData);
      Notification.mockReturnValueOnce(newNotification);

      const createdNotification = await notificationService.sendNotification(notificationData);

      expect(Notification).toHaveBeenCalledWith(notificationData);
      expect(newNotification.save).toHaveBeenCalled();
      expect(createdNotification).toEqual(newNotification);
    });
  });

  describe('getNotifications', () => {
    it('should get notifications for a recipient', async () => {
      const recipientId = '123456789';
      const notifications = [new Notification({ message: 'Test message 1', type: 'info', recipient: recipientId }), new Notification({ message: 'Test message 2', type: 'warning', recipient: recipientId })];
      Notification.find.mockResolvedValueOnce(notifications);

      const result = await notificationService.getNotifications(recipientId);

      expect(result).toEqual(notifications);
    });
  });
});