const courseService = require('../src/services/courseService');
const Course = require('../src/models/course');

jest.mock('../src/models/course');

describe('Course Service', () => {
  beforeEach(() => {
    Course.mockClear();
  });

  describe('createCourse', () => {
    it('should create a new course', async () => {
      const courseData = { name: 'Test Course', code: 'TEST101', credits: 3 };
      const newCourse = new Course(courseData);
      Course.mockReturnValueOnce(newCourse);

      const createdCourse = await courseService.createCourse(courseData);

      expect(Course).toHaveBeenCalledWith(courseData);
      expect(newCourse.save).toHaveBeenCalled();
      expect(createdCourse).toEqual(newCourse);
    });
  });

  describe('getCourses', () => {
    it('should get all courses', async () => {
      const courses = [new Course({ name: 'Test Course 1', code: 'TEST101', credits: 3 }), new Course({ name: 'Test Course 2', code: 'TEST102', credits: 4 })];
      Course.find.mockResolvedValueOnce(courses);

      const result = await courseService.getCourses();

      expect(result).toEqual(courses);
    });
  });

  describe('getCourseById', () => {
    it('should get a course by ID', async () => {
      const courseId = '123456789';
      const course = new Course({ name: 'Test Course', code: 'TEST101', credits: 3 });
      Course.findById.mockResolvedValueOnce(course);

      const result = await courseService.getCourseById(courseId);

      expect(Course.findById).toHaveBeenCalledWith(courseId);
      expect(result).toEqual(course);
    });

    it('should throw an error if course is not found by ID', async () => {
      const courseId = '123456789';
      Course.findById.mockResolvedValueOnce(null);

      await expect(courseService.getCourseById(courseId)).rejects.toThrow('Course not found');
    });
  });

    describe('updateCourse', () => {
        it('should update an existing course', async () => {
        const courseId = '123456789';
        const courseData = { name: 'Test Course', code: 'TEST101', credits: 3 };
        const updatedCourse = new Course(courseData);
        Course.findByIdAndUpdate.mockResolvedValueOnce(updatedCourse);
    
        const result = await courseService.updateCourse(courseId, courseData);
    
        expect(Course.findByIdAndUpdate).toHaveBeenCalledWith(courseId, courseData, { new: true, runValidators: true });
        expect(result).toEqual(updatedCourse);
        });
    
        it('should throw an error if course is not found during update', async () => {
        const courseId = '123456789';
        const courseData = { name: 'Test Course', code: 'TEST101', credits: 3 };
        Course.findByIdAndUpdate.mockResolvedValueOnce(null);
    
        await expect(courseService.updateCourse(courseId, courseData)).rejects.toThrow('Course not found');
        });
    });

    describe('deleteCourse', () => {
        it('should delete an existing course', async () => {
        const courseId = '123456789';
        const deletedCourse = new Course({ _id: courseId });
        Course.findByIdAndDelete.mockResolvedValueOnce(deletedCourse);
    
        const result = await courseService.deleteCourse(courseId);
    
        expect(Course.findByIdAndDelete).toHaveBeenCalledWith(courseId);
        expect(result).toEqual(deletedCourse);
        });
    
        it('should throw an error if course is not found during deletion', async () => {
        const courseId = '123456789';
        Course.findByIdAndDelete.mockResolvedValueOnce(null);
    
        await expect(courseService.deleteCourse(courseId)).rejects.toThrow('Course not found');
        });
    });

    describe('assignFaculty', () => {
        it('should assign faculty to a course', async () => {
        const courseId = '123456789';
        const faculty = '123456789';
        const course = new Course({ _id: courseId });
        Course.findById.mockResolvedValueOnce(course);
    
        const result = await courseService.assignFaculty(courseId, faculty);
    
        expect(Course.findById).toHaveBeenCalledWith(courseId);
        expect(course.faculty).toEqual(faculty);
        expect(course.save).toHaveBeenCalled();
        expect(result).toEqual(course);
        });
    
        it('should throw an error if course is not found during faculty assignment', async () => {
        const courseId = '123456789';
        const faculty = '123456789';
        Course.findById.mockResolvedValueOnce(null);
    
        await expect(courseService.assignFaculty(courseId, faculty)).rejects.toThrow('Course not found');
        });
    });
});