const facultyService = require('../src/services/facultyService');
const Faculty = require('../src/models/faculty');

jest.mock('../src/models/faculty');

describe('Faculty Service', () => {
  beforeEach(() => {
    Faculty.mockClear();
  });

  describe('createFaculty', () => {
    it('should create a new faculty member', async () => {
      const facultyData = { name: 'Software Engineering', department: 'IT', designation: 'Sub', email: 'se@example.com', phoneNumber: '1234567890' };
      const newFaculty = new Faculty(facultyData);
      Faculty.mockReturnValueOnce(newFaculty);

      const createdFaculty = await facultyService.createFaculty(facultyData);

      expect(Faculty).toHaveBeenCalledWith(facultyData);
      expect(newFaculty.save).toHaveBeenCalled();
      expect(createdFaculty).toEqual(newFaculty);
    });
  });

  describe('getFaculties', () => {
    it('should get all faculty members', async () => {
      const faculties = [new Faculty({ name: 'Software Engineering', department: 'IT', designation: 'Sub', email: 'se@example.com', phoneNumber: '1234567890' }), new Faculty({ name: 'Electrical Engineering', department: 'Engineering', designation: 'Sub', email: 'ee@example.com', phoneNumber: '9876543210' })];
      Faculty.find.mockResolvedValueOnce(faculties);

      const result = await facultyService.getFaculties();

      expect(result).toEqual(faculties);
    });
  });

  describe('getFacultyById', () => {
    it('should get a faculty member by ID', async () => {
      const facultyId = '123456789';
      const faculty = new Faculty({ name: 'Software Engineering', department: 'IT', designation: 'Sub', email: 'se@example.com', phoneNumber: '1234567890' });
      Faculty.findById.mockResolvedValueOnce(faculty);

      const result = await facultyService.getFacultyById(facultyId);

      expect(Faculty.findById).toHaveBeenCalledWith(facultyId);
      expect(result).toEqual(faculty);
    });

    it('should throw an error if faculty member is not found by ID', async () => {
      const facultyId = '123456789';
      Faculty.findById.mockRejectedValueOnce(new Error('Faculty not found'));

      await expect(facultyService.getFacultyById(facultyId)).rejects.toThrow('Faculty not found');
    });
  });

  describe('updateFaculty', () => {
    it('should update an existing faculty member', async () => {
      const facultyId = '123456789';
      const facultyData = { name: 'Software Engineering', department: 'IT', designation: 'Sub', email: 'se@example.com', phoneNumber: '1234567890' };
      const updatedFaculty = new Faculty(facultyData);
      Faculty.findByIdAndUpdate.mockResolvedValueOnce(updatedFaculty);

      const result = await facultyService.updateFaculty(facultyId, facultyData);

      expect(Faculty.findByIdAndUpdate).toHaveBeenCalledWith(facultyId, facultyData, { new: true, runValidators: true });
      expect(result).toEqual(updatedFaculty);
    });

    it('should throw an error if faculty member is not found during update', async () => {
      const facultyId = '123456789';
      const facultyData = { name: 'Software Engineering', department: 'IT', designation: 'Sub', email: 'se@example.com', phoneNumber: '1234567890' };
      Faculty.findByIdAndUpdate.mockRejectedValueOnce(new Error('Faculty not found'));

      await expect(facultyService.updateFaculty(facultyId, facultyData)).rejects.toThrow('Faculty not found');
    });
  });

  describe('deleteFaculty', () => {
    it('should delete an existing faculty member', async () => {
      const facultyId = '123456789';
      const deletedFaculty = new Faculty({ _id: facultyId });
      Faculty.findByIdAndDelete.mockResolvedValueOnce(deletedFaculty);

      const result = await facultyService.deleteFaculty(facultyId);

      expect(Faculty.findByIdAndDelete).toHaveBeenCalledWith(facultyId);
      expect(result).toEqual(deletedFaculty);
    });

    it('should throw an error if faculty member is not found during deletion', async () => {
      const facultyId = '123456789';
      Faculty.findByIdAndDelete.mockRejectedValueOnce(new Error('Faculty not found'));

      await expect(facultyService.deleteFaculty(facultyId)).rejects.toThrow('Faculty not found');
    });
  });
});
