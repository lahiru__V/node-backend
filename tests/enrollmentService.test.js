const enrollmentService = require('../src/services/enrollmentService');
const Enrollment = require('../src/models/enrollment');

jest.mock('../src/models/enrollment');

describe('Enrollment Service', () => {
  beforeEach(() => {
    Enrollment.mockClear();
  });

  describe('enrollStudent', () => {
    it('should enroll a student in a course', async () => {
      const enrollmentData = { student: '123456789', course: '987654321' };
      const newEnrollment = new Enrollment(enrollmentData);
      Enrollment.mockReturnValueOnce(newEnrollment);

      const createdEnrollment = await enrollmentService.enrollStudent(enrollmentData);

      expect(Enrollment).toHaveBeenCalledWith(enrollmentData);
      expect(newEnrollment.save).toHaveBeenCalled();
      expect(createdEnrollment).toEqual(newEnrollment);
    });
  });

  describe('dropStudent', () => {
    it('should drop a student from a course', async () => {
      const studentId = '123456789';
      const courseId = '987654321';
      const deletedEnrollment = new Enrollment({ student: studentId, course: courseId });
      Enrollment.findOneAndDelete.mockResolvedValueOnce(deletedEnrollment);

      const result = await enrollmentService.dropStudent(studentId, courseId);

      expect(Enrollment.findOneAndDelete).toHaveBeenCalledWith({ student: studentId, course: courseId });
      expect(result).toEqual(deletedEnrollment);
    });

    it('should throw an error if enrollment is not found', async () => {
      const studentId = '123456789';
      const courseId = '987654321';
      Enrollment.findOneAndDelete.mockResolvedValueOnce(null);

      await expect(enrollmentService.dropStudent(studentId, courseId)).rejects.toThrow('Enrollment not found');
    });
  });
});