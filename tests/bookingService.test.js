const bookingService = require('../src/services/bookingService');
const Booking = require('../src/models/booking');

jest.mock('../src/models/booking');

describe('Booking Service', () => {
  beforeEach(() => {
    Booking.mockClear();
  });

  describe('bookRoomResource', () => {
    it('should create a new booking', async () => {
      const bookingData = { resource: 'Room 101', date: new Date(), time: '10:00' };
      const newBooking = new Booking(bookingData);
      Booking.mockReturnValueOnce(newBooking);

      const createdBooking = await bookingService.bookRoomResource(bookingData);

      expect(Booking).toHaveBeenCalledWith(bookingData);
      expect(newBooking.save).toHaveBeenCalled();
      expect(createdBooking).toEqual(newBooking);
    });
  });

  describe('updateBooking', () => {
    it('should update an existing booking', async () => {
      const bookingId = '123456789';
      const bookingData = { resource: 'Room 101', date: new Date(), time: '12:00' };
      const updatedBooking = new Booking(bookingData);
      Booking.findByIdAndUpdate.mockResolvedValueOnce(updatedBooking);

      const result = await bookingService.updateBooking(bookingId, bookingData);

      expect(Booking.findByIdAndUpdate).toHaveBeenCalledWith(bookingId, bookingData, { new: true, runValidators: true });
      expect(result).toEqual(updatedBooking);
    });

    it('should throw an error if booking is not found during update', async () => {
      const bookingId = '123456789';
      const bookingData = { resource: 'Room 101', date: new Date(), time: '12:00' };
      Booking.findByIdAndUpdate.mockResolvedValueOnce(null);

      await expect(bookingService.updateBooking(bookingId, bookingData)).rejects.toThrow('Booking not found');
    });
  });

  describe('cancelBooking', () => {
    it('should cancel an existing booking', async () => {
      const bookingId = '123456789';
      const cancelledBooking = new Booking({ _id: bookingId });
      Booking.findByIdAndDelete.mockResolvedValueOnce(cancelledBooking);

      const result = await bookingService.cancelBooking(bookingId);

      expect(Booking.findByIdAndDelete).toHaveBeenCalledWith(bookingId);
      expect(result).toEqual(cancelledBooking);
    });

    it('should throw an error if booking is not found during cancelation', async () => {
      const bookingId = '123456789';
      Booking.findByIdAndDelete.mockResolvedValueOnce(null);

      await expect(bookingService.cancelBooking(bookingId)).rejects.toThrow('Booking not found');
    });
  });
});