const express = require('express');
const connectDB = require('./src/db/connection');
const userRoutes = require('./src/routes/userRoutes');
const courseRoutes = require('./src/routes/courseRoutes');
const timetableRoutes = require('./src/routes/timetableRoutes');
const bookingRoutes = require('./src/routes/bookingRoutes');
const enrollmentRoutes = require('./src/routes/enrollmentRoutes');
const notificationRoutes = require('./src/routes/notificationRoutes');
const facultyRoutes = require('./src/routes/facultyRoutes');

const app = express();

app.use(express.json());

connectDB();

app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);
app.use('/api/timetable', timetableRoutes);
app.use('/api/booking', bookingRoutes);
app.use('/api/enrollment', enrollmentRoutes);
app.use('/api/notification', notificationRoutes);
app.use('/api/faculty', facultyRoutes);

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Internal server error');
});

module.exports = app;
