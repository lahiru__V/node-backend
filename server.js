const app = require('./app');
const config = require('./src/config/default.json');

app.listen(config.port, () => {
  console.log(`Server running on port ${config.port}`);
});